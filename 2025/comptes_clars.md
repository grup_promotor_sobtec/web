---
layout: default
title: "Els comptes clars"
---

# {{page.title}}

Persones inscrites: **76** [26.02.2025]

Persones inscrites al dinar: **42**

Persones apuntades al servei de canguratge: **1**

## Romanent [SobTec 2024](https://sobtec.cat/2024/comptes_clars.html)
 197,99 €

## Despeses del SobTec 2025

| Concepte | &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cost |
| :---- | ---:|
| Servidor, etc | 126,50 € |
| Lleialtat (espai, consergeria, etc) | 129,21 € |
| Dinars | ~ 336, 00 € |
| Pastes | ~ 120,00 € | 
| Compra (llets, fruita, sucres, sucs, etc.) | 51,77 €| 
| **Total** | **~ 711,71 €** | 

 <!--platans: 3,67€
mandarina: 3,15€
pomes: 5,58€
llet: 7,5€
sucs: 19,94€
suc civada: 2,98€
sucre: 2,35€
tovallons: 6,6€

total 51,77€-->

## Ingressos del Sobtec 2025

| Concepte &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;| &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cost |
| :---- | ---:|
| Dinars | ~ 336, 00 € |
| Donacions       | ?|
| **Total** | **?** |

## Romanent Sobec 2026


