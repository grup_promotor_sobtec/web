---
layout: programa
title: "Xarxes comunitàries sense fils a Catalunya: experiències i novetats"
ponent: "Associació Expansió de la Xarxa Oberta (eXO) i guifi·net."
ponent-link: https://guifi.net
ponent-link: https://guifi.net
ponent-description: "<a href='https://exo.cat/'>L'Associació Expansió de la Xarxa Oberta (eXo)</a> és una associació sense ànim de lucre que des de 2010 promou les xarxes de telecomunicacions comunitàries, la sobirania tecnològica i la reducció de l’escletxa digital. L’associació cuida activament el proveïment d’accés a Internet per a les sòcies. També manté i amplia bona part de la xarxa comunitària a Barcelona.<br/><br/><a href='https://guifi.net'>Guifi.net</a> és un projecte tecnològic, social i econòmic impulsat des de la ciutadania que té per objectiu la creació d'una xarxa de telecomunicacions oberta, lliure i neutral basada en un model de comuns. El desenvolupament d'aquesta infraestructura mancomunada facilita l'accés a les telecomunicacions en general i a la connexió a Internet de banda ampla en particular, de qualitat, a un preu just i per a tothom. A més, genera un model d'activitat econòmica col·laborativa, sostenible i de proximitat."
ponent-mastodont:
ponent-link: 
ponent-twitter: 
tags: 
  - Connectivitat
  - Infraestructura digital
  - Xarxes sense fils comunitàries
  - Autogestió
  - Xarxes descentralitzades
pad: 
teixidora: 
draft: false
hour: "15:40"
pad: https://pad.femprocomuns.cat/sobtec2025-guifi-net-exo
room: "Sala de baix"
---

Parlarem del desenvolupament de xarxes comunitàries sense fils a Catalunya, amb exemples recents de projectes en barris de Barcelona i a Castellar del Vallès. Es parlarà de l’experiència d’[eXO.cat](https://exo.cat) en la promoció d’una connectivitat oberta i col·laborativa, així com del paper de guifi.net en el desplegament d’infraestructures compartides.
Explorarem iniciatives com [GuifiCastellar](https://castellar.guifi.net/guifinet) i el projecte [Digicoria](https://canodrom.barcelona/en/community/digicoria) del Canòdrom, que impulsen models de xarxa autogestionats i de proximitat. També es debatran els reptes i les oportunitats d’aquestes xarxes, així com la seva importància per a una internet més lliure i descentralitzada.
Una oportunitat per conèixer experiències reals i connectar amb la comunitat!