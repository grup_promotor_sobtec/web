---
title: "Taller: Repte sense mòbil, en família"
layout: programa
ponent: "Mercè Botella, autora de <i>Repte sense mòbil: per a famílies</i>"
ponent-description: "Mercè Botella és psicòloga social i sòcia fundadora de Som Connexió, la cooperativa de telefonia i internet conscient i sense afany de lucre. També és mare. La perspectiva que li donen les tres mirades (mare, psicòloga i fundadora de l'única telecos que porta el consum conscient a l'ADN), la van portar a escriure les tres guies que actualment té Som Connexió per ajudar les famílies en l'ús responsable de la tecnologia: <i>Guia per a famílies cruels i malvades</i>, <i>Despantalla'm</i> i <i>Repte sense mòbil: per a famílies</i>. "
ponent-mastodont:
ponent-link: 
ponent-twitter: null
draft: false
tags:
  - Educació a les pantalles
  - Benestar i pantalles
hour: "10:30"
room: "Sala de baix"
permalink: /2025/:slug
pad: https://pad.femprocomuns.cat/sobtec2025-taller-repte-sense-pantalles
---

Taller per a famílies (adults del nucli familiar i menors que ja fan ús del mòbil) que té per objectiu que reflexionin conjuntament sobre l’ús abusiu de la tecnologia. Impartit per la Mercè Botella, autora de la guia [Repte sense mòbil: per a famílies](https://somconnexio.coop/educacio/arriba-el-repte-sense-mobil-per-a-families/), us donarem eines per portar a terme una proposta senzilla i valenta alhora: el repte que tota la família passeu uns dies sense mòbil ni pantalles! Les famílies assistents al taller, rebran un exemplar imprès gratuït de la guia.


> Aquesta activitat és un taller, pensat per, com a **màxim 50 persones**. En general no hi hauria d'haver-hi problema d'aforament, però si voleu assegurar la vostra plaça, podeu reservar a través d'[aquest formulari](https://nextcloud.pangea.org/apps/forms/s/8nXsx9pEC2mWyXwcANtN8zQ2).