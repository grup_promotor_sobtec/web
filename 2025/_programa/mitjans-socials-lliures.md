---
layout: programa
title: "Descobrim els mitjans socials lliures"
ponent: "Fadelkon, Titi i Marcelcosta de La Furgo"
ponent-description: "<a href='https://sindominio.net/lafurgo/'>La Furgo</a> és una campanya per facilitar un trasllat organitzat des de les xarxes socials comercials a les autogestionades."
ponent-mastodont: "https://bcn.fedi.cat/lafurgo"
ponent-link: https://sindominio.net/lafurgo/
ponent-twitter: 
tags: 
  - xarxes socials
  - fediverse
pad: 
teixidora: 
draft: false
hour: "19:05"
pad: https://pad.femprocomuns.cat/sobtec2025-mitjans-socials-lliures
room: "Sala de dalt"
---


Taller on l’equip de [la Furgo](https://sindominio.net/lafurgo/) presentarà la seva campanya per traslladar-nos a les xarxes socials lliures. Coneixerem què és el Fedivers, quines són les avantatges d’un espai digital autogestionat i quines eines en formen part. Després, per grups més petits, explorarem les eines, com accedir-hi i aprofitarem per resoldre dubtes o curiositats. 