---
layout: programa
title: "Quina internet volem? Decidim-ho!"
ponent: Participants del grup promotor
ponent-description: 
ponent-mastodont:
ponent-link: 
ponent-twitter: 
tags: 
  - apoderament digital
  - sobirania d'internet
pad: 
teixidora: 
draft: false
hour: "16:30"
pad: https://pad.femprocomuns.cat/sobtec2025-governan%C3%A7a-digital
room: "Sala de dalt"
---

La internet és una construcció humana, la fem entre totes les persones, i li podem donar forma. D'això va el parlar de la governança d'internet, de donar-li forma entre totes. Quins temes ens importen? Com volem que sigui? Un precedent per aquest procés a Catalunya es la deliberació per arribar a les  [Estratègies per l'apoderament digital](https://apoderamentdigital.cat/). Seguint el model del Fòrum de Governança d’Internet, volem implicar al món de la solidaritat i social ("part interessada" en termes de l'IGF). Hem començat un diàleg per entendre i triar les qüestions i reptes a tractar, parlar sobre direccions i accions, arribar a un consens, redactar i publicar i posar en marxa una agenda local o regional comuna, vinculada a processos regionals i globals, cap a una Internet per a totes i tots. Volem comptar amb tu en aquest procés.