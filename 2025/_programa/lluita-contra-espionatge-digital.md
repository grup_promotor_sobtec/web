---
layout: programa
title: "Taula Rodona: La lluita contra l'espionatge digital"
ponent: "Adriana Ribas d'Amnistia Internacional, Andreu Van den Eynde d'Irídia, i Joan Arús de Sentinel Alliance"
ponent-description: 
ponent-mastodont: 
ponent-link: 
ponent-twitter: 
draft: false
tags:
 - Espionatge Digital
 - Pegasus
 - Vigilància
hour: "17:55"
pad: https://pad.femprocomuns.cat/sobtec2025-espionatge-digital
room: "Sala de dalt"
permalink: /2025/:slug
---

En els últims anys, la tecnologia s'està convertint en una arma més per a violar els drets humans. Amb l'excusa de la seguretat, governs de tot el món compren i permeten la venda de programes com Pegasus per a espiar, assetjar i intimidar a defensors i defensores dels drets humans, periodistes i activistes, posant en perill innombrables vides. Governs i empreses diuen que les seves eines de vigilància s'utilitzen només en contra de delinqüents i terroristes. Però la realitat és una altra: s'han utilitzat de manera il·legítima contra defensors i defensores dels drets humans de tot el món. Ningú està fora de perill.
