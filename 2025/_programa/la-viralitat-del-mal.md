---
layout: programa
title: "La Viralitat del Mal"
ponent: "Proyecto UNA"
ponent-description: "Proyecto UNA és un col·lectiu antifa cuqui que investiga la nostra relació amb la tecnologia i les noves formes de propaganda online."
ponent-mastodont: 
ponent-link: 
ponent-twitter: 
draft: false
tags: 
  - xarxes socials
  - odi
hour: "13:10"
permalink: /2025/:slug
pad: https://pad.femprocomuns.cat/sobtec2025-la-viralitat-del-mal
type: full-afternoon
---

La Viralitat del Mal és la nostra forma d'entendre les relacions entre el Big Tech i les noves formes de reaccionarisme polític, i amb la que intentem donar claus per poder transformar la situació en la que vivim.

---

**El Proyecto una és un col·lectiu anònim i ens han demanat que no es facin fotos ni vídeos durant la seva xerrada, però no tenen problemes en què enregistrem les seves veus.**