---
layout: programa
title: "Què fa la Unió Europea per  un futur digital sobirà"
ponent: Cristian Bulumac
ponent-description: "Cristian Bulumac és Assessor de Polítiques Tecnològiques al Parlament Europeu"
ponent-mastodont: 
ponent-link: 
ponent-twitter: 
draft: false
hour: "10:30"
room: "Sala de dalt"
tags: 
  - 
permalink: /2025/:slug
pad: https://pad.femprocomuns.cat/sobtec2025-sobirania-tecnologica-a-europa
warning: "Xerrada en anglès amb traducció parcial al català"
---

La Unió Europea a vegades sembla una institució llunyana i abstracta, amb mecanismes complexos que estan desconnectats de la nostra vida quotidiana. Què passa a Bruxelles, com es prenen les decisions, per qui i amb quines conseqüències per a la nostra sobirania digital? Quin és el significat d'abreviatures com DSA DMA AIA i com pot millorar aquests actes legislatius la sobirania individual?

**Xerrada en anglès amb traducció parcial al català**