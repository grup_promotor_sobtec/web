---
layout: programa
title: "Benvinguda i presentació"
ponent: ""
ponent-description: "Grup Promotor del SobTec, La Teixidora i Mobile Social Congress"
ponent-mastodont: 
ponent-link: https://www.teixidora.net/
ponent-twitter: 
draft: false
hour: "10:00"
type: full-morning
pad: https://pad.femprocomuns.cat/sobtec2025-benvinguda
room: Sala de Dalt
permalink: /2025/:slug
---

_Presentació del programa i petita introducció a [La Teixidora](https://www.teixidora.net/) i com prenem apunts col·laborativament, el que ens ajuden a documentar el SobTec. A més presentarem les activitats del [Mobile Social Congress](https://mobilesocialcongress.cat/) de la setmana després del SobTec_