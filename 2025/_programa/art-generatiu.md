---
layout: programa
title: "Art generatiu en l’era de la blockchain"
ponent: "Heey i Daniel Julià"
ponent-description: "<a href='https://heeey.art'>heey</a> heeey és un arquitecte i artista generatiu de Barcelona. Mitjançant algoritmes crea sistemes artístics en els que explora idees i conceptes a través del color i l'expressió geomètrica.<br/><br/>
<a href='https://kiwoo.org'>Daniel Julià</a> és enginyer de telecomunicacions, desenvolupador d’aplicacions web, docent universitari i artista generatiu a temps parcial."
ponent-mastodont:
ponent-link:
ponent-twitter: 
tags: 
  - art generatiu
  - art dels mtijans
  - blockchain
  - long form generative art
pad: https://pad.femprocomuns.cat/sobtec2025-art-generatiu
teixidora: 
draft: false
hour: "12:15"
room: "Sala de dalt"
---

En aquesta xerrada explorarem la intersecció entre l'art generatiu i la blockchain, centrant-nos en com aquestes tecnologies permeten noves formes de creació i distribució artística. A través d'exemples pràctics i la nostra experiència en el “long form generative art”, analitzarem les possibilitats que ofereix aquest mitjà per a la sobirania digital dels artistes, mostrant obres destacades i eines que estan transformant el panorama de l'art digital.