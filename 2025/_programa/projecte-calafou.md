---
layout: programa
title: "El projecte de Calafou"
ponent: "Enriqueta, Marta i Ari de Calafou"
ponent-description:
ponent-mastodont:
ponent-link: 
ponent-twitter: 
tags: 
  - utopia
pad: 
teixidora: 
draft: false
hour: "15:40"
pad: https://pad.femprocomuns.cat/sobtec2025-projecte-calafou
room: "Sala de dalt"
---

Des de 2011 Calafou, una antiga colònia industrial a Vallbona d’Anoia, proposa alternatives sostenibles i respectuoses amb l’entorn al model de producció, de tecnologia i d’habitatge. Després d'explicar com funciona Calafou es presentaran els projectes _Precious Plastic_ i _Futurotopies, laberints i forats_. 


## Futurotopies, laberints i forats

La presentació es planteja la viabilitat del pensament utòpic, a partir del terme plural futurotopies com a fil conductor. Fil d'Ariadna que ens durà a parlar del les diferents formes de la temporalitat, de les utopies i distopies, del laberint com a imatge global de tot plegat i dels forats com possibilitat i garant de la respiració de l'esperança.

[El projecte a la Wiki-fou](https://wiki.calafou.org//index.php/Futurotopias_Varias)

[Projecte la Sènia Amagada](https://wiki.calafou.org/index.php/La_S%C3%A8nia_Amagada)