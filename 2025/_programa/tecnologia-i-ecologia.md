---
layout: programa
title: "Per què no vols un centre de dades a la teva regió?"
ponent: "Luis García Valverde y Anna Mundet Molas"
ponent-description: "Luis García Valverde i Anna Mundet Molas, activistes d'Ecologistes en Acció"
ponent-mastodont: 
ponent-link: 
ponent-twitter: 
draft: false
tags:
  - Ecologia
  - Sostenibilitat dels centres de dades

hour: "16:30"
type: 
permalink: /2024/:slug
pad: https://pad.femprocomuns.cat/sobtec2025-tecnologia-i-ecologia
room: "Sala de baix"
---

Els data centers són una infraestructura de la qual últimament sentim a parlar molt, però de la que sabem molt poc. En aquesta xerrada explicarem què és un centre de dades i què implica tenir-ne un al costat de casa. Analitzarem el seu impacte ambiental, posant el focus en el consum energètic, l’ús d’aigua i les externalitats ecològiques que comporten. A partir del cas d’Aragó, on Ecologistes en Acció ha impulsat al·legacions contra macroprojectes de centres de dades, veurem com ens podem organitzar per defensar el territori.

