---
layout: default
title: "Inscriu-te al SobTec"
---

# Inscriu-t'hi si vols venir!

El SobTec és gratuït, però poder preveure quanta gent vindrà ens facilita molt les coses que la gent s'apunti. En cas que se superés l'aforament d'una sala, es donaria prioritat a les persones inscrites.

<h1>Inscripcions</h1>
<p>El SobTec és gratuït, però poder preveure quanta gent vindrà ens facilita molt les coses que la gent s'apunti. En cas que se superés l'aforament d'una sala, es donaria prioritat a les persones inscrites.</p>
 <div style="text-align:center">
<a href="https://nextcloud.pangea.org/apps/forms/s/XNBF8yFdHXBGKkznckzJEzNM" class="button">Inscriu-t'hi al SobTec 2025</a>
</div>

<h2>Dinar</h2>
<p>El dinar costarà com a molt 10 euros (probablement al voltant de 8 €), que s'hauran de pagar en efectiu. Serà un menú vegà i sense gluten. Tancarem el formulari <b>DILLUNS, 24 de febrer al matí</b>.</p>
<div style="text-align:center">
<a href="https://nextcloud.pangea.org/apps/forms/s/5yN3JQMsWFtoKrGxiRSRHqfE" class="button">Formulari de dinars</a>
</div>

<h2>Canguratge</h2>
<p>No podem prometre res, però, si hi hagués interès, intentarem oferir servei de canguratge durant les xerrades. Per això necessitaríem saber si hi ha prou interès, a quines franges horàries i per a quines edats.</p>
<div style="text-align:center">
    <a href="https://nextcloud.pangea.org/apps/forms/s/acnHTDyYxtFpa88fZEDmEsBF" class="button">Formulari de canguratge</a>
</div>



# Vols col·laborar amb nosaltres?

Hi ha moltes maneres de col·laborar amb el SobTec {{site.year}}. Aquí en proposem algunes, però estem oberts a qualsevol proposta:

## Ajuda'ns a fer possible el SobTec {{site.year}}!
Els dies abans, durant i després del SobTec és quan hi ha més feina i més ajuda necessitem. Per organitzar-nos, repartim les tasques en grups: Apuntat al que vulguis!

* **Logística:**
Ens ajuda a muntar i desmuntar l'espai, moure taules i cadires i preparar la part audiovisual. La part grossa de la feina serà **divendres tarda, i dissabte matí i tarda**. Clar que si cada persona que ve ens ajuda una mica, acabarem abans...  


* **Prendre apunts amb la teixidora:**
Porta el teu ordinador i pren apunts col·laboratius de les xerrades, que després pujarem a [La Teixidora](https://www.teixidora.net/). A la presentació i després d'algunes pauses explicarem com fer-ho, però és molt fàcil i divertit... ah, i no cal tenir un català perfecte ;)  


* **Estar al punt d'informació:**
Durant el Sobtec, hem de fer torns per tenir sempre un parell de persones al punt d'informació, però farem tot el possible perquè també puguis seguir les xerrades des d'allà. A més hauràs de mirar que no falti cafè, té, begudes, etc.  


## Ajuda'ns a fer difusió
També ens pots ajudar fent difusió a través de les xarxes socials: utilitzem el hashtag <b>#SobTec{{site.year}}</b>. 

També pots enviar això (o una variació d'això) a través d'altres plataformes:

{% include program-snippet.html %}


## Fer una donació
També pots fer una donació (com a persona o col·lectiu) a la guardiola que posarem al punt d'informació. 

_El SobTec es finança, en gran part, a través d'aquestes donacions_ 

Ves a [Els comptes clars](/comptes_clars.html)

## Forma part del Grup Promotor

Aquí tens més informació sobre com [formar part del Grup Promotor]({{site.baserul}}/grup-promotor)
