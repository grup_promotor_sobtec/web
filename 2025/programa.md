---
layout: default
title: "Programa"
---

<h1>Programa</h1>

{% include programa.html %}

<div class="divider"></div>
<a href="https://nextcloud.pangea.org/s/J3n7c3r7GtMEXR4"> Presentacions</a>
<h2>I a més... Sessió PD amb música lliure! </h2>

Us tenim una proposta que no us podeu perdre! 

Durant les pauses i l'estona d'enxarxament del VII Congrés de Sobirania Tecnològica, <a href="
https://blog.anartist.org/omar/">El Generador d'Envolupant</a> us oferirà una sessió de DJ molt especial, amb música d'artistes que formen part del Fedivers i que comparteixen les seves obres sota llicències Creative Commons.


<h2 class="no-print">Si en voleu més... <a href="https://mobilesocialcongress.cat/">Mobile Social Congress!</a></h2>

<p class="no-print">El 3, 4 i 5 de març podeu apuntar-vos al <a href="https://mobilesocialcongress.cat/">Mobile Social Congress</a>, un espai obert a la ciutadania, que ofereix una mirada crítica a l'actual model de producció i consum d'aparells electrònics i de les tecnologies de la informació i la comunicació, i una oportunitat per conèixer models i projectes alternatius basats en la sobirania tecnològica, la sostenibilitat i el respecte pels drets humans.</p>

<p class="no-print">Podeu apuntar-vos <a href="https://nextcloud.pangea.org/apps/forms/s/wFjwWWZmCBMCmgrpyisEw5nA">en aquest formulari</a></p> 

<a href="https://mobilesocialcongress.cat/"><img src="/assets/img/Banner_MSC_info.webp" alt="Bàner del Mobile Social Congress on s'anuncia el programa, que podeu trobar a sota. A la dreta, hi ha el lema Hackegem el sistema, construïm resistències, i al costat un ordinador del qual surten uns ratolins." class="no-print" width="512px" style="width:75%;margin-left: auto;margin-right:auto; display: block;"></a>