---
layout: default
title: "Inscriu-te"
---

## Inscriu-te al Congrés

No és obligatoria la inscripció però sí que ens ajuda a preveure quanta gent vindrà.
En cas que se superés l'aforament d'una sala, es donaria prioritat a les persones inscrites.

Només cal omplir aquest [formulari](https://formularis.sobtec.cat/inscripcions-sobtec-2020)


