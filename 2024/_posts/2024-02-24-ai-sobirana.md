---
layout: programa
title: "Una intel·ligència artificial sobirana? Poden ser nostres els datasets, les màquines, i els models que s'utilitzen?"
ponent: Bruno Vianna
ponent-description: |
  Bruno Vianna forma part de <a href="https://canodrom.barcelona/ca/comunitat/digicoria">Digicoria</a>, projecte que busca impulsar la descentralització de la web, a través de tallers, discussions, documentació i producció de material pedagògic.
ponent-mastodont: 
ponent-link: https://canodrom.barcelona/ca/comunitat/digicoria
ponent-twitter: 
draft: false
type: full-morning
hour: "10:45"
permalink: /2024/:slug
pad: https://pad.femprocomuns.cat/sobtec4-ia-sobirana-20240302
video: https://archive.org/details/xerrada-1-una-intel.ligencia-artificial-sobirana-poden-ser-nostres-els-datasets-
teixidora: https://www.teixidora.net/wiki/Una_intel%C2%B7lig%C3%A8ncia_artificial_sobirana%3F_Poden_ser_nostres_els_datasets,_les_m%C3%A0quines,_i_els_models_que_s%27utilitzen%3F_2024/03/02
tags:
  - intel·ligència artificial
  - datasets

---

En aquesta xerrada explicarem alguns conceptes bàsics perquè tothom pugui entendre què és i fer-se una idea de com funciona la intel·ligència artificial. Això ens permetrà discutir sobre la sobirania i les dependències que es generen al voltant d'aquesta tecnologia així com reflexionar críticament sobre les promeses i els riscos del seu ús per a la participació i transformació social.