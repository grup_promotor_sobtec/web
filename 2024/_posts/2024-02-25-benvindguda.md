---
layout: programa
title: "Benvinguda i presentació"
ponent: 
ponent-description: "."
ponent-mastodont: 
ponent-link:
ponent-twitter: 
draft: false
permalink: /2024/:slug
hour: "10:15"
type: full-morning
pad: https://pad.femprocomuns.cat/sobtec4-benvinguda-20240302
video: https://archive.org/details/benvinguda-i-presentacio-apunts-2024
teixidora: https://www.teixidora.net/wiki/Benvinguda_i_presentaci%C3%B3_2024/03/02


---

Presentació del Sobtec 2024