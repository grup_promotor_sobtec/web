---
layout: programa
title: "Eines digitals per a entitats o projectes: El problema de menystenir el manteniment"
ponent: Procomuns i Víctor Fancelli Capdevila
ponent-description: |
  <a href="https://femprocomuns.coop">Procomuns.coop</a> són una cooperativa integral de treball i usuàries, entre les quals més de 60 són entitats. Des del 2017, amb SomNúvol.coop mancomunen eines de programari lliue per consolidar un ecosistema procomú, basat en els principis del cooperativisme obert, l'autogestió comunitària, la sostenibilitat ecològica, econòmica i humana, el coneixement compartit i la replicabilitat.
ponent-mastodont: 
ponent-link: 
ponent-twitter: 
draft: false
tags:
  - Infraestructura digital
  - manteniment
  
hour: "13:05"
type: 
permalink: /2024/:slug
pad: https://pad.femprocomuns.cat/Eines-dig-20240302
video: https://archive.org/details/xerrada-3-eines-digitals-per-a-entitats-o-projectes-el-problema-de-menystenir-el-manteniment
teixidora: https://www.teixidora.net/wiki/Eines_digitals_per_a_entitats_o_projectes:_El_problema_de_menystenir_el_manteniment_2024/03/02
---

És molt difícil que un col·lectiu pugui desenvolupar la seva feina sense eines digitals, per compartir la informació necessària per desenvolupar el seu projecte. Per a molts projectes, però, és un problema utilitzar una infraestructura digital (el conjunt d'eines digitals) que no contradigui les seves idees: per desconeixement o per motius econòmics, utilitzen eines aparentment gratuïtes sense adonar-se que delegar el manteniment d'aquesta infraestructura  implica establir una dependència amb les empreses i el seu model de negoci (capitalisme de vigilància) que hi ha al darrere. 

A la xerrada s'explicarà quins són els problemes de menystenir el manteniment de les infraestructures digitals i farem un repàs de quines eines resolen necessitats organitzatives pel que fa al treball en equip, la comunicació interna i externa o la gestió econòmica des d'una perspectiva tecnoètica.
