---
title: "Dispositius mòbils lliures: Reptes i avanços 2024"
layout: programa
ponent: Brian Russell de Degoogled
ponent-description: "Amb seu a Barcelona, Degoogled treballa amb una comunitat creixent per promocionar tecnologies igualitàries, transparents i democràtiques. A més, organitza tallers per  desgooglejar dispositius mòbils i  ofereix telèfons intel·ligents rectificats, és a dir, amb sistemes operatius que respecten la llibertat, privacitat i seguretat de les persones usuàries."
ponent-mastodont:
ponent-link: https://www.degoogled.es/
ponent-twitter: null
draft: false
tags: 
  - smartphone
  - desgooglejar

hour: "15:30"
permalink: /2024/:slug
pad: https://pad.femprocomuns.cat/smartphones-20240302
video: https://archive.org/details/xerrada-4-dispositius-mobils-lliures-reptes-i-avancos-2024
teixidora: https://www.teixidora.net/wiki/Dispositius_m%C3%B2bils_lliures:_Reptes_i_avan%C3%A7os_2024_2024/03/02
---

Quines opcions té un consumidor avui dia de maquinari i programari lliure en un dispositiu mòbil? 
En aquesta xerrada, farem un repàs dels pros i contres de SO lliures que es poden instal·lar en dispositius Android ([lineage](https://lineageos.org/), [/e/](https://e.foundation), [iodé](https://iode.tech), [microG](https://microg.org/), [CalyxOS](https://calyxos.org/), [GrapheneOS](https://grapheneos.org/)), als linux phones, fairphones, etc., així com dels punts de venda dels dispositius lliures, etc. Proposarem una sèrie de consells de com trobar un equilibri entre seguretat, privadesa i comoditat i economia segons les necessitats de diferents usuaris. Taller a càrrec de Brian Russell de [degoogled.es]({{page.ponent-link}})