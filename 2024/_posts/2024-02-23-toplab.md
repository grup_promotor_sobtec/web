---
layout: programa
title: "Programació en directe de música i visualitzacions. Programari lliure per una acció artística sobirana!"
ponent: Toplap Barcelona
ponent-description: "TOPLAP Barcelona és un col·lectiu que practica i promou el live coding com a tècnica de creació sonora i visual, generant una apropiació tecnològica a través de l’ús i desenvolupament de programari lliure i obert enfocat a generar relacions i discursos propis."
ponent-mastodont: 
ponent-link: https://hangar.org/ca/orbitants-duracionals/toplap/
ponent-twitter: 
draft: false
type: "full-afternoon"
hour: "18:45"
tags: 
  - Programació en directe
permalink: /2024/:slug
pad: https://pad.femprocomuns.cat/Top-lap-20240302
video: https://archive.org/details/live-coding_202502
teixidora: https://www.teixidora.net/wiki/Programaci%C3%B3_en_directe_de_m%C3%BAsica_i_visualitzacions._Programari_lliure_per_una_acci%C3%B3_art%C3%ADstica_sobirana!_2024/03/02
---

La _Programació en directe_ (_live coding_) és una disciplina artística que combina elements de les arts escèniques i l'acció artística amb l'escriptura de codi, que genera sons, imatges o altres efectes. El públic veu no només el resultat de la programació, sinó també el codi mateix i com els artistes l'escriuen i el modifiquen.

Per tancar el Sobtec 2024 a Hangar, el col·lectiu TOPLAP Barcelona presentarà un espectacle que reflexiona sobre el programari lliure i la sobirania tecnològica a l'acció artística. Amb la participació de:
* Penny
* QBRNTHSS
* Alfonsofono
* Savamala
* Yanú Phlux
* Loopier
