---
layout: default
title: "Els comptes clars"
---

# {{page.title}}

Persones apuntades: 65


## Despeses del congrés

| Concepte | Cost |
| :---- | ---:|
| Dinar dels ponents (8€ x4) | 32,00 € |
| Faltaven pot cerveses (2,5 x4) | 10,00 € |
| WCloc | 292,82 €  |
| Cartells | 60,81 € |
| Pastes | 90,96 € | 
| Compra Supermercat | 65,87 € | 
| Cafès | 75,00 € |
| Gots | 12,00 € |
| Neteja | 38,72 € | 
| **Total** | **678,19 €** | 

## Ingressos del congrés

| Donació OpenVote  | 678,19 € | 
| **Total** | 678,19 € |

## Donacions Infraestructura i SobTec 2025

| Per al proper any | 197,99 € |

