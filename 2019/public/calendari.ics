BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//Meetup//RemoteApi//EN
CALSCALE:GREGORIAN
METHOD:PUBLISH
X-ORIGINAL-URL:http://sobtec.cat/calendari.ics
X-WR-CALNAME:Sobtec.ics
X-MS-OLK-FORCEINSPECTOROPEN:TRUE
BEGIN:VTIMEZONE
TZID:Europe/Madrid
X-LIC-LOCATION:Europe/Madrid
BEGIN:STANDARD
TZOFFSETFROM:+0200
TZOFFSETTO:+0100
TZNAME:CET
DTSTART:19701025T030000
RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU
END:STANDARD
END:VTIMEZONE

BEGIN:VEVENT
CLASS:PUBLIC
UID:benvinguda
SUMMARY:BENVINGUDA I PRESENTACIÓ DEL CONGRÉS
DESCRIPTION:Presentarem el Congrés 2019 conjuntament amb els companys de <a href='http://booleans.cat/'>Booleans</a> que aquest any han col·laborat en l'organització
DTSTAMP:20180215T113846Z
DTSTART;TZID=Europe/Madrid:20190302T100000
DTEND;TZID=Europe/Madrid:20190302T103000
GEO:41.37284;2.13745
LOCATION:La Lleialtat Santsenca (Carrer d'Olzinelles\, 31\, 08014 Barcelona)
URL:
END:VEVENT

BEGIN:VEVENT
CLASS:PUBLIC
UID:xerrada_1
SUMMARY:Estratègies municipals per a l'apoderament digital
DESCRIPTION:Presentació de les 46 Mesures per a l’apoderament digital als municipis. Eines i recursos per als ajuntaments <a href='https://apoderamentdigital.cat/'>apoderamentdigital.cat</a>
DTSTAMP:20180215T113846Z
DTSTART;TZID=Europe/Madrid:20190302T103000
DTEND;TZID=Europe/Madrid:20190302T113000
GEO:41.37284;2.13745
LOCATION:La Lleialtat Santsenca (Carrer d'Olzinelles\, 31\, 08014 Barcelona)
URL:
END:VEVENT

BEGIN:VEVENT
CLASS:PUBLIC
UID:xerrada_2
SUMMARY:Treball en plataformes digitals: noves formes de precarietat laboral
DESCRIPTION:L'objecte de la xerrada és analitzar el treball en plataformes digitals com Uber, Deliveroo, Glovo, Amazon Mechanical Turk o Upwork i les conseqüències laborals que genera, des d'una perspectiva de la consideració dels treballadors com a treballadors assalariats o treballadors autònoms i des d'una perspectiva de la qualitat del treball.
(<span class="speaker-info" data-toggle="tooltip" data-placement="left" title="Professora de Dret del Treball.">Anna Ginès</span>)

DTSTAMP:20180215T113846Z
DTSTART;TZID=Europe/Madrid:20190302T113000
DTEND;TZID=Europe/Madrid:20190302T123000
GEO:41.37284;2.13745
LOCATION:La Lleialtat Santsenca (Carrer d'Olzinelles\, 31\, 08014 Barcelona)
URL:
END:VEVENT

BEGIN:VEVENT
CLASS:PUBLIC
UID:xerrada_3
SUMMARY:Sobre la llibertat d'expressió i com podem afavorir-la amb tecnologies descentralitzades
DESCRIPTION:Com censuren els governs certes pàgines web? Com poden les tecnologies descentralitzades emergents ajudar a eludir aquesta censura de continguts? En aquesta xerrada veurem els mecanismes tècnics que utilitzen alguns règims autoritaris per controlar la informació a la que tenen accés els seus ciutadans, utilitzant com a exemples el bloqueig global de Wikileaks el 2010 i el bloqueig de Wikipedia a Turquia el 2017.
(<span class="speaker-info" data-toggle="tooltip" data-placement="left" title="Investigador del projecte P2P Models a la Universitat Complutense de Madrid, estudia tecnologies descentralitzades en el context de les comunitats del procomú.">David Llop Vila</span>)

DTSTAMP:20180215T113846Z
DTSTART;TZID=Europe/Madrid:20190302T113000
DTEND;TZID=Europe/Madrid:20190302T123000
GEO:41.37284;2.13745
LOCATION:La Lleialtat Santsenca (Carrer d'Olzinelles\, 31\, 08014 Barcelona)
URL:
END:VEVENT

BEGIN:VEVENT
CLASS:PUBLIC
UID:descans0
SUMMARY:DESCANS
DESCRIPTION:
DTSTAMP:20180215T113846Z
DTSTART;TZID=Europe/Madrid:20190302T123000
DTEND;TZID=Europe/Madrid:20190302T130000
GEO:41.37284;2.13745
LOCATION:La Lleialtat Santsenca (Carrer d'Olzinelles\, 31\, 08014 Barcelona)
URL:
END:VEVENT

BEGIN:VEVENT
CLASS:PUBLIC
UID:xerrada_4
SUMMARY:Biaixos i discriminació en l'anàlisi predictiu basat en aprenentatge automàtic
DESCRIPTION:La idea de facilitar -i de vegades substituir- decisions humanes amb processos d'intel·ligència artificial pren cada cop més força a àmbits ben variats: assegurances, serveis financers, campanyes polítiques, justícia, policia... En aquesta ponència argumentarem 1) com aquests sistemes d'aprenentatge automàtic generen preocupants situacions de discriminació i 2) que aquests biaixos no es poden solucionar de forma automàtica i és necessari el seguiment humà per detectar-los i eliminar-los.
(<span class="speaker-info" data-toggle="tooltip" data-placement="left" title="Professor de Filosofia de la ciència i la tecnologia a la UAB. La seva línia de recerca actual són els impactes socials i cognitius de les TIC i ha publicat diversos llibres i articles sobre el tema.">David Casacuberta</span>)

DTSTAMP:20180215T113846Z
DTSTART;TZID=Europe/Madrid:20190302T130000
DTEND;TZID=Europe/Madrid:20190302T140000
GEO:41.37284;2.13745
LOCATION:La Lleialtat Santsenca (Carrer d'Olzinelles\, 31\, 08014 Barcelona)
URL:
END:VEVENT

BEGIN:VEVENT
CLASS:PUBLIC
UID:xerrada_5
SUMMARY:Recepta per activistes: munta una web anònima i resistent a la censura
DESCRIPTION:En contextos d'activisme (social, laboral, etc.) de vegades és necessari comunicar informació sense revelar la nostra identitat. Igualment, per evitar la censura, ens interessa poder replicar i restaurar la nostra web de forma ràpida. Veurem solucions a aquestes qüestions (i a d'altres relacionades) des d'una perspectiva pràctica.
(<span class="speaker-info" data-toggle="tooltip" data-placement="left" title="Físic per formació. Programador de professió. Sindicalista per obligació. Activista de programari lliure per vocació.">Carlos Pascual</span>)

DTSTAMP:20180215T113846Z
DTSTART;TZID=Europe/Madrid:20190302T130000
DTEND;TZID=Europe/Madrid:20190302T140000
GEO:41.37284;2.13745
LOCATION:La Lleialtat Santsenca (Carrer d'Olzinelles\, 31\, 08014 Barcelona)
URL:
END:VEVENT

BEGIN:VEVENT
CLASS:PUBLIC
UID:dinar
SUMMARY:DINAR
DESCRIPTION:Posarem una taula al mateix espai del Congrés per poder dinar totes juntes. Us podeu <a href="http://vine.sobtec.cat" target="_blank">apuntar</a> al pastís de verdures (o carn) o portar el vostre menjar.

DTSTAMP:20180215T113846Z
DTSTART;TZID=Europe/Madrid:20190302T140000
DTEND;TZID=Europe/Madrid:20190302T151500
GEO:41.37284;2.13745
LOCATION:La Lleialtat Santsenca (Carrer d'Olzinelles\, 31\, 08014 Barcelona)
URL:
END:VEVENT

BEGIN:VEVENT
CLASS:PUBLIC
UID:xerrada_6
SUMMARY:Estònia, estat digital. Realitat o ficció?
DESCRIPTION:Diuen que Estònia és la primera nació digital del món. Què hi ha darrera d'aquesta afirmació? Un conte de fades o una nació realment digital? Desentranyem la infrastructura tècnica d'Estònia, les seves possibilitats i què en podem aprendre.
(<span class="speaker-info" data-toggle="tooltip" data-placement="left" title="Regidor a l'Ajuntament d'Igualada per Decidim Igualada. Contribueix activament al programari lliure i a la cultura lliure, havent estat administrador del directori d'obres alliberades de l'Estat espanyol, Qomun.">Dario Castañé</span>)

DTSTAMP:20180215T113846Z
DTSTART;TZID=Europe/Madrid:20190302T173000
DTEND;TZID=Europe/Madrid:20190302T183000
GEO:41.37284;2.13745
LOCATION:La Lleialtat Santsenca (Carrer d'Olzinelles\, 31\, 08014 Barcelona)
URL:
END:VEVENT

BEGIN:VEVENT
CLASS:PUBLIC
UID:xerrada_7
SUMMARY:Desenvolupant programari lliure des de la comunitat per la societat: KDE i Open Food Network
DESCRIPTION:En aquesta presentació veurem com s'organitzen les dues comunitats de programari lliure: KDE i Open Food Network. Repassarem quins són els seus objectius principals, com s'organitzen, quines eines fan servir, com es financen i com podem contribuir-hi.
(<span class="speaker-info" data-toggle="tooltip" data-placement="left" title="Desenvolupador i fundador de Coopdevs, organització catalana que té com a objectiu potenciar l'economia social i solidària amb programari lliure i pràctiques àgils.">Pau Pérez</span>) i
(<span class="speaker-info" data-toggle="tooltip" data-placement="left" title="Aleix Pol i Gonzàlez has been collaborating with KDE since 2007. He started working in software development in the KDE Education area and KDevelop. Aleix joined the KDE e.V. board of directors in 2014. In his day-job, he is employed by BlueSystems where he has worked with other parts of the community including Plasma and Qt.">Aleix Pol</span>)

DTSTAMP:20180215T113846Z
DTSTART;TZID=Europe/Madrid:20190302T151500
DTEND;TZID=Europe/Madrid:20190302T161500
GEO:41.37284;2.13745
LOCATION:La Lleialtat Santsenca (Carrer d'Olzinelles\, 31\, 08014 Barcelona)
URL:
END:VEVENT

BEGIN:VEVENT
CLASS:PUBLIC
UID:xerrada_8
SUMMARY:Història i estat actual de les xarxes socials lliures i distribuïdes
DESCRIPTION:Actualment les xarxes federades representen una alternativa sòlida a les xarxes propietàries. En aquesta xerrada es repassarà la història d'aquestes xarxes i com es plantegen el seu futur més immediat.
<a href="https://fediverse.party/">fediverse.party</a>
(chris i kim)

DTSTAMP:20180215T113846Z
DTSTART;TZID=Europe/Madrid:20190302T161500
DTEND;TZID=Europe/Madrid:20190302T171500
GEO:41.37284;2.13745
LOCATION:La Lleialtat Santsenca (Carrer d'Olzinelles\, 31\, 08014 Barcelona)
URL:
END:VEVENT

BEGIN:VEVENT
CLASS:PUBLIC
UID:xerrada_9
SUMMARY:Protecció de la privacitat i dades obertes. Alguns exemples
DESCRIPTION:Presentarem alguns exemples d'atacs de reidentificació que s’han portat a terme amb dades de registres mèdics a Massachussets, cerques en AOL, avaluacions de pel·lícules a Netflix i dades geolocalitzades de taxistes a NYC, per motivar la discussió sobre el concepte de dades de caràcter personal i la seva relació amb l'anonimat. Discutirem el delicat balanç entre benefici i risc de les dades obertes amb privacitat.
(<span class="speaker-info" data-toggle="tooltip" data-placement="left" title="Doctor en matemàtiques aplicades, actualment es dedica a l'estudi de tècniques per a la protecció de dades, des de bases de dades estadístiques, fins a xarxes socials i dades de mobilitat.">Julián Salas</span>)

DTSTAMP:20180215T113846Z
DTSTART;TZID=Europe/Madrid:20190302T161500
DTEND;TZID=Europe/Madrid:20190302T171500
GEO:41.37284;2.13745
LOCATION:La Lleialtat Santsenca (Carrer d'Olzinelles\, 31\, 08014 Barcelona)
URL:
END:VEVENT

BEGIN:VEVENT
CLASS:PUBLIC
UID:descans1
SUMMARY:DESCANS
DESCRIPTION:
DTSTAMP:20180215T113846Z
DTSTART;TZID=Europe/Madrid:20190302T171500
DTEND;TZID=Europe/Madrid:20190302T173000
GEO:41.37284;2.13745
LOCATION:La Lleialtat Santsenca (Carrer d'Olzinelles\, 31\, 08014 Barcelona)
URL:
END:VEVENT

BEGIN:VEVENT
CLASS:PUBLIC
UID:xerrada_10
SUMMARY:Abusos i reptes de la T-Mobilitat
DESCRIPTION:Què és la T-Mobilitat? Per què s'ha portat amb tant de secretisme i després de més de quatre anys de la concessió no s'ha fet cap campanya per difondre en què consisteix aquest projecte de tanta rellevància? Per què incomoda tant polítics com empreses de parlar-ne?
Des de la <a href="https://t-mobilitat.info/">Plataforma ciutadana de transparència per la T-Mobilitat</a> t'explicarem des d'una òptica multidimensional alguns dels problemes principals que té el projecte. I, en particular, la violació de la privacitat que representarà la vigilància massiva de tots els usuaris del transport públic.

DTSTAMP:20180215T113846Z
DTSTART;TZID=Europe/Madrid:20190302T151500
DTEND;TZID=Europe/Madrid:20190302T161500
GEO:41.37284;2.13745
LOCATION:La Lleialtat Santsenca (Carrer d'Olzinelles\, 31\, 08014 Barcelona)
URL:
END:VEVENT

BEGIN:VEVENT
CLASS:PUBLIC
UID:xerrada_11
SUMMARY:Xarxa Comunitària per l'Internet de les Coses
DESCRIPTION:L'Internet de les Coses ens ofereix moltes oportunitats per monitoritzar el nostre entorn, reduir el consum, la contaminació i compartir béns materials. A la vegada, però, és una amenaça feroç a la privacitat, el dret a la intimitat i l'anonimat. La xarxa comunitària The Things Network està construint una xarxa oberta, lliure i neutral per apoderar la ciutadania i aprofitar col·lectivament les tecnologies per a usos socials.
(<span class="speaker-info" data-toggle="tooltip" data-placement="left" title="maker i desenvolupador de software i electrònica lliure">Xose Pérez</span>)

DTSTAMP:20180215T113846Z
DTSTART;TZID=Europe/Madrid:20190302T173000
DTEND;TZID=Europe/Madrid:20190302T183000
GEO:41.37284;2.13745
LOCATION:La Lleialtat Santsenca (Carrer d'Olzinelles\, 31\, 08014 Barcelona)
URL:
END:VEVENT

BEGIN:VEVENT
CLASS:PUBLIC
UID:xerrada_12
SUMMARY:La reconquesta del món virtual: sobirania
DESCRIPTION:Som en una època en què les imatges i les dades, que compartim a través de tota mena de dispositius tecnològics, condicionen la nostra vida privada i determinen la dinàmica de l’esfera pública i de les comunitats. Podríem dir que el món ja és una interfície virtual. En aquest context digital liderat pel capitalisme neoliberal, el temps cronològic esdevé cronoscòpic, l’individu un ésser quantificat, l’experiència immediata una experiència mediada i datificada, i la biosfera una infosfera. Després del diagnòstic del lloc que ocupa el subjecte en aquest context, arriba la reconquesta.
(<span class="speaker-info" data-toggle="tooltip" data-placement="left" title="productora cultural, professora i assagista.">Ingrid Guardiola</span>)

DTSTAMP:20180215T113846Z
DTSTART;TZID=Europe/Madrid:20190302T183000
DTEND;TZID=Europe/Madrid:20190302T193000
GEO:41.37284;2.13745
LOCATION:La Lleialtat Santsenca (Carrer d'Olzinelles\, 31\, 08014 Barcelona)
URL:
END:VEVENT

END:VCALENDAR
